package com.sarinityclasses.radioButtonexmple;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class Assertion {

	@Test
	public void hardAssertExample() {

		System.setProperty("webdriver.chrome.driver", "D:\\chromenew\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://courses.letskodeit.com/practice");
		String actualTitle = driver.getTitle();
		String expectedTitle = "Pracatice Page";
		SoftAssert s_assert=new SoftAssert();
	Assert.assertEquals(actualTitle, expectedTitle);
		s_assert.assertEquals(actualTitle, expectedTitle);
		System.out.println("The titles are the same");
		s_assert.assertAll();
		driver.quit();

	}

}

// assert is used as for condition checking
//when we use soft assert we have to create the object of softassert
//if softassert is fail then other excution will be proceed and atest case faile msg coming
// if use assert all then if any condition is fail then test case also failed.

