package com.sarinityclasses.radioButtonexmple;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class AnnotationExample {
	@Test
	public void Annotationexample() {
		System.out.println("i am in tc");
	}
	@Test
	public void Annotationexample2() {
		System.out.println("i am in tc2");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("i an in before method");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("i an in after method");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("i an in before class");

	}

	@AfterClass
	public void afterClass() {
		System.out.println("i an in after class");

	}
	

	@BeforeTest
	public void beforeTest() {
		System.out.println("i an in before test");

	}

	@AfterTest
	public void afterTest() {
		System.out.println("i an in after test");

	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("i an before suite");

	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("i an in after suite ");

	}

}
