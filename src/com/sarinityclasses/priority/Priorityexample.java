package com.sarinityclasses.priority;

import org.testng.annotations.Test;

public class Priorityexample {
	@Test(priority = -1,enabled = false)
	public void Test1() {
		System.out.println("In test case 1");

	}

	      @Test(priority = 0)
	public void Test2() {
		System.out.println("In test case 2");
	}

	@Test(priority = -1,dependsOnMethods = ("Test4"))
	public void Test3() {
		System.out.println("In test case 3");

	}

	@Test(priority = 2)
	public void Test4() {
		System.out.println("In test case 4");

	}

}
