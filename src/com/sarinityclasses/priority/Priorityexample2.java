package com.sarinityclasses.priority;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Priorityexample2 {
	@Test(priority = -1,enabled = false)
	public void Test1() {
		System.out.println("In test case 1");

	}

	@Test(priority = 0,groups = {"sanity"})
	public void Test2() {
		System.out.println("In test case 2");

	}

	@Test(priority = -2)
	public void Test3() {
		System.out.println("In test case 3");

	}

	@Test(priority = 2,dependsOnMethods = {"Test2","Test3"})
	public void Test4() {
		System.out.println("In test case 4");

	}
	@Test
	@Parameters({"valueone","valuetwo"})
	public void Test5(int valueone,int valuetwo) {
		System.out.println(valueone+valuetwo);
	}

}
